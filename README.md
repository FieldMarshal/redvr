# REDVR 
*(**R**ealtime **E**ngineering **D**ecision VR)*

**OVERVIEW**
Application that allows engineers to view and modify models in real time in order to make engineering decisions using Virtual Reality (VR).


**GOALS**

 - Real Time Decision Making
 - Use real life scale
 - User friendly
 - Visualization interaction that cannot be done with traditional maquette/video solutions

**SPECIFICATIONS**
Application that allows engineers to view and modify models in real time in order to make engineering decisions.


Design Document:
https://docs.google.com/document/d/13Pja2lVEbDCUVOF8-B37MWYtkuWAWdv6S1wgKjsoKuY/edit#


The following software version are required to compile this repo:

    Unreal Engine 4.24
    Oculus VR plug-in

For debugging and running on the Oculus quest see the instructions in the [Oculus Documentation](https://developer.oculus.com/documentation/unreal/unreal-quick-start-guide-quest/?locale=en_US&device=QUEST).
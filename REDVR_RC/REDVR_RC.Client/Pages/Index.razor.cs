﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MatBlazor;

namespace REDVR_RC.Client.Pages
{
    public partial class Index
    {
        [Inject]
        NavigationManager NavigationManager { get; set; }

        //TODO: Change to default FALSE when not in development
        public bool IsConnected = true;

        private List<string> colorValues = new List<string>();

        private string colorInput;

        private string Color { get => colorInput; set => SetColor(value); }

        MatTheme theme1 = new MatTheme()
        {
            Primary = MatThemeColors.Orange._500.Value,
            Secondary = MatThemeColors.BlueGrey._500.Value
        };

        protected override async Task OnInitializedAsync()
        {

        }

        public void SetColor(string color)
        {
            colorInput = color;

            if (IsConnected)
            {
                Console.WriteLine("Sent Color change to " + colorInput);
                colorValues.Add(color);
                theme1.Primary = colorInput;
                StateHasChanged();
            }

        }
    }
}

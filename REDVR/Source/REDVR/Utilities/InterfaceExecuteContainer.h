﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "UObject/Object.h"

/**
 *  Container to hold pointer to interface and Object to be executed
 */

template <typename T> 
struct InterfaceExecuteContainer
{
	T* InterfaceForExecuting = nullptr;
	UObject* ObjectToExecuteOn = nullptr;

	InterfaceExecuteContainer(T* Interface, UObject* Obj) : InterfaceForExecuting(Interface), ObjectToExecuteOn(Obj)
	{
		
	}

	
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "REDTool.h"

// Sets default values
AREDTool::AREDTool()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AREDTool::SetGrabbed()
{
	IsGrabbed = true;
}

void AREDTool::SetReleased()
{
	IsGrabbed = false;
}

// Called when the game starts or when spawned
void AREDTool::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AREDTool::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


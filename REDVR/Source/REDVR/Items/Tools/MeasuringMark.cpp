// Fill out your copyright notice in the Description page of Project Settings.

#include "MeasuringMark.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AMeasuringMark::AMeasuringMark()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Setup General Root 
	USceneComponent * Root = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Marker"));
	RootComponent = Root;

	//Line beam connecting the marks
	ConnectionBeam = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ConnectionBeam"));
	ConnectionBeam->SetupAttachment(RootComponent);

	//Text showing distance of line with default rotation
	LineDistanceTxt = CreateDefaultSubobject<UTextRenderComponent>(TEXT("DistanceTxt"));
	LineDistanceTxt->SetupAttachment(ConnectionBeam);
	LineDistanceTxt->SetRelativeRotation(FRotator(90.f, 90.f, 0.f));


}


// Called when the game starts or when spawned
void AMeasuringMark::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMeasuringMark::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(LastMark != nullptr)
	{
		DistanceToLastMark = GetDistanceToLastMark();

		PointBeamToLastMark(DistanceToLastMark);
	}

}

void AMeasuringMark::SetNextMark(AActor* NextCreatedMark)
{
	NextMark = NextCreatedMark;
}

void AMeasuringMark::SetLastMark(AActor* PreviousMark)
{
	if (PreviousMark)
	{
		LastMark = PreviousMark;
	}
}

float AMeasuringMark::GetDistanceToLastMark()
{
	DistanceToLastMark = (LastMark->GetActorLocation() - GetActorLocation()).Size();
	
	return DistanceToLastMark;
}

FRotator AMeasuringMark::GetLookAtRotation()
{
	FVector directionVector = LastMark->GetActorLocation() - GetActorLocation();
	
	return directionVector.Rotation();
}


void AMeasuringMark::PointBeamToLastMark()
{
	ConnectionBeam->SetWorldScale3D(FVector(GetDistanceToLastMark(),
		BeamScaleValue,
		BeamScaleValue
	));

	ConnectionBeam->SetWorldRotation(FRotator(GetLookAtRotation()));
}

void AMeasuringMark::PointBeamToLastMark(const float Distance)
{
	ConnectionBeam->SetWorldScale3D(FVector( DistanceToLastMark,
        BeamScaleValue,
        BeamScaleValue
    ));

	ConnectionBeam->SetWorldRotation(FRotator(GetLookAtRotation()));
}



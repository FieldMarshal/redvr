// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MeasuringMark.h"
#include "Shape.h"
#include "MeasuringTriangle.generated.h"

UCLASS()
class REDVR_API AMeasuringTriangle : public AMeasuringMark, public Shape
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMeasuringTriangle();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	//Shape
    virtual void SetCoordinates(float xcor, float ycor) override;
    
    virtual  float GetYCoordinate() override;
    
    virtual float GetXCoordinate() override;
    	  
    virtual void Draw() override;

};

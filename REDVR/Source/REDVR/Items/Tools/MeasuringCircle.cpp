// Fill out your copyright notice in the Description page of Project Settings.


#include "MeasuringCircle.h"

// Sets default values
AMeasuringCircle::AMeasuringCircle() : AMeasuringMark()
{
	//Line beam connecting the marks
	CircleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CircleMesh"));
	CircleMesh->SetupAttachment(RootComponent);

}


void AMeasuringCircle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (LastMark != nullptr)
	{
		Draw();
	}
}

void AMeasuringCircle::SetCoordinates(float xcor, float ycor)
{
	x = xcor;
	y = ycor;
}

float AMeasuringCircle::GetYCoordinate()
{
	return y;
}

float AMeasuringCircle::GetXCoordinate()
{
	return x;
}

void AMeasuringCircle::Draw()
{
	//Convert float distance to int
	int currentRadius = GetDistanceToLastMark();

	SetRadius(currentRadius);

	CircleMesh->SetWorldScale3D(FVector(radius,
		radius,
		upScale
	));

}

void AMeasuringCircle::SetRadius(float r)
{
	radius = r / 100;
}

float AMeasuringCircle::GetReadius()
{
	return radius;
}



class Shape
{
	virtual void SetCoordinates(float xcor, float ycor) = 0;
	
	virtual float GetYCoordinate() = 0;

	virtual float GetXCoordinate() = 0;

	virtual void Draw() = 0;

};

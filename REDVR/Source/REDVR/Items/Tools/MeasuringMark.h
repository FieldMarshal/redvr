// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/TextRenderComponent.h"
#include "MeasuringMark.generated.h"

UCLASS()
class REDVR_API AMeasuringMark : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Property")
		float DistanceToLastMark = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Property")
		float BeamScaleValue = 1.75;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Measuring")
		UTextRenderComponent* LineDistanceTxt =  nullptr;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Measuring")
		AActor* LastMark = nullptr;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Measuring")
		AActor* NextMark = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Measuring")
		UStaticMeshComponent* ConnectionBeam;





public:	
	// Sets default values for this actor's properties
	AMeasuringMark();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	void SetNextMark(AActor* NextCreatedMark);

	void SetLastMark(AActor* PreviousMark);

	float GetDistanceToLastMark();

	FRotator GetLookAtRotation();

private:
	void PointBeamToLastMark(float Distance);
	
	void PointBeamToLastMark();

};


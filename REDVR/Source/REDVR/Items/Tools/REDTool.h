// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "REDTool.generated.h"

UCLASS()
class REDVR_API AREDTool : public AActor
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Interactions")
	bool IsGrabbed;

public:	
	// Sets default values for this actor's properties
	AREDTool();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void SetGrabbed();
	
	UFUNCTION(BlueprintCallable)
	void SetReleased();

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MeasuringMark.h"
#include "Shape.h"
#include "MeasuringCircle.generated.h"

UCLASS()
class REDVR_API AMeasuringCircle : public AMeasuringMark, public Shape
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Element")
		UStaticMeshComponent* CircleMesh;

public:	
	// Sets default values for this actor's properties
	AMeasuringCircle();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Shape
	virtual void SetCoordinates(float xcor, float ycor) override;

	virtual  float GetYCoordinate();

	virtual float GetXCoordinate();
	  
	virtual void Draw();

	void SetRadius(float r);

	virtual float GetReadius();

private:
	float x, y, radius = 0;

	float upScale = 0.05f;

};

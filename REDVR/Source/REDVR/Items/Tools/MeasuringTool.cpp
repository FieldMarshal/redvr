// Fill out your copyright notice in the Description page of Project Settings.

#include "MeasuringTool.h"
#include "MeasuringMark.h"
#include "Engine/World.h"


AMeasuringTool::AMeasuringTool()
{
	MarkerAttachPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Marker Attach Point"));
	MarkerAttachPoint->SetupAttachment(RootComponent);
}

float AMeasuringTool::GetTotalDistance()
{
	float Distance = 0.f;

	for (AMeasuringMark* Mark : DistanceMarks)
	{
		if(Mark->LastMark)
		{
			Distance += Mark->GetDistanceToLastMark();
		}
	}

	return Distance;
}

void AMeasuringTool::CreateMarker()
{
	if(isMarkerDropped)
	{
		GrabbedMark->LastMark = GrabbedMark;

		GrabbedMark = GetWorld()->SpawnActor<AMeasuringMark>(
			AMeasuringMark::StaticClass(), GetActorLocation(),
			FRotator::ZeroRotator);

		DistanceMarks.Add(GrabbedMark);

		GrabbedMark->AttachToComponent(MarkerAttachPoint, FAttachmentTransformRules::KeepWorldTransform);

		isMarkerDropped = false;
	}
	
}

void AMeasuringTool::PlaceMarker()
{
	GrabbedMark->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	GrabbedMark = nullptr;
}

void AMeasuringTool::RemoveMarker(AMeasuringMark* SelectedMark)
{
	SelectedMark->Destroy();
}


void AMeasuringTool::RemoveAllMarkers()
{
	for (AMeasuringMark* Mark : DistanceMarks)
	{
		Mark->Destroy();
	}
}

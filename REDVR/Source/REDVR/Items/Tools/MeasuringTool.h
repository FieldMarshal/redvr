// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "MeasuringMark.h"
#include "MeasuringTool.generated.h"

/**
 * 
 */
UCLASS()
class REDVR_API AMeasuringTool : public AStaticMeshActor
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Property")
		float TotalDistance;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Property")
		bool isMarkerDropped = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Element")
		TArray<AMeasuringMark*> DistanceMarks;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Element")
		USceneComponent* MarkerAttachPoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Element")
		AMeasuringMark* GrabbedMark;



public:
	AMeasuringTool();

	UFUNCTION(BlueprintCallable, Category = "Data")
		float GetTotalDistance();

	UFUNCTION(BlueprintCallable, Category = "Functionality")
		void CreateMarker();

	UFUNCTION(BlueprintCallable, Category = "Functionality")
		void PlaceMarker();

	UFUNCTION(BlueprintCallable, Category = "Functionality")
		void RemoveMarker(AMeasuringMark * SelectedMark);

	UFUNCTION(BlueprintCallable, Category = "Functionality")
		void RemoveAllMarkers();


	
};

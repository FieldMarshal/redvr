// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/ActorComponent.h"
#include "MCAbility.generated.h"



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class REDVR_API UMCAbility : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMCAbility();
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Element")
	USceneComponent* LeftHandMC;
    
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Element")
	USceneComponent* RightHandMC;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Element")
	APawn* MCOwnerPawn;

	FName AbilityName;
		
};

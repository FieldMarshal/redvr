// Fill out your copyright notice in the Description page of Project Settings.


#include "MCScale.h"

#include "Kismet/KismetSystemLibrary.h"

// Sets default values for this component's properties
UMCScale::UMCScale()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
	AbilityName = FName("Scale Ability MCScale");
}


void UMCScale::TickAbility_Implementation()
{
	if(IsTargetScalable && TargetActorToScale != nullptr)
	{
		ScaleTarget(TargetActorToScale);
	}
}

FName UMCScale::GetName_Implementation()
{
	return AbilityName;
}

bool UMCScale::SetMC_Implementation(USceneComponent* LeftHand, USceneComponent* RightHand)
{
	if(LeftHand && RightHand)
	{
		RightHandMC = RightHand;

		LeftHandMC = LeftHand;

		return true;
	}
	else
	{
		return false;
	}
}

// Called when the game starts
void UMCScale::BeginPlay()
{
	Super::BeginPlay();

	// ...
}

void UMCScale::SetTargetToScale(AActor* targetToScale)
{	
	if(targetToScale != nullptr )
	{
		if( targetToScale == TargetActorToScale)
		{
			TargetOriginalScaleVector = TargetActorToScale->GetActorScale3D();
		
			IsTargetScalable = true;
		}
		else
		{
			TargetActorToScale = targetToScale;
		}	
	}
	else
	{
		TargetActorToScale = nullptr;
		IsTargetScalable = false;
	}

	
}

void UMCScale::ScaleTarget(USceneComponent* targetToScale)
{
	if(!IsTargetScalable) return;
	
	const float HandToHandDist = FVector::Dist(LeftHandMC->GetComponentLocation(),  RightHandMC->GetComponentLocation());
	
	const float ScaleRatio = 1.0f;
	
	// Scale Object by hand separation distance.
	const float ScaleFactor =  (HandToHandDist * ScaleRatio);
	
	// Scene Component
	
	if(targetToScale != nullptr)
	{
		targetToScale->SetWorldScale3D(TargetOriginalScaleVector * ScaleFactor);
		
	}
	else
	{
		IsTargetScalable = false;
	}
	 
}

void UMCScale::ScaleTarget(AActor* targetToScale)
{
	if(!IsTargetScalable) return;

	float HandToHandDist = FVector::Dist(LeftHandMC->GetComponentLocation(),  RightHandMC->GetComponentLocation());
	
	//UE_LOG(LogTemp, Warning, TEXT("Distance: %f"), HandToHandDist);
	
	if(OriginalHandDistance == 0.0f)
	{
		OriginalHandDistance = HandToHandDist;
	}

	const float ScaleRatio = 1.0f;
	
	// Scale Object by hand separation distance.
	const float ScaleFactor =  ((HandToHandDist / OriginalHandDistance) * ScaleRatio);

	// Actor
	if(targetToScale != nullptr)
	{
		targetToScale->SetActorScale3D(TargetOriginalScaleVector * ScaleFactor);
		
	}
	else
	{
		IsTargetScalable = false;
	}
}

void UMCScale::OnGrabActor_Implementation(USceneComponent* MotionController, AActor* GrabbedActor)
{
	UE_LOG(LogTemp, Warning, TEXT("CALLED ON GRAB ACTOR FUNCITON WITH PARAMETER"));
	
}

void UMCScale::OnGrab_Implementation(USceneComponent* MotionController)
{
	UE_LOG(LogTemp, Warning, TEXT("CALLED ON GRAB FUNCITON WITH PARAMETER"));
	//TODO: Implement
}


void UMCScale::OnGrabRelease_Implementation(USceneComponent* MotionController)
{
	UE_LOG(LogTemp, Warning, TEXT("CALLED ON GRAB RELEASE FUNCITON WITH PARAMETER"));
	//TODO: Implement
}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "MCAbilityGrabber.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UMCAbilityGrabber : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class REDVR_API IMCAbilityGrabber
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
	
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category=MCAbility)
    void OnGrab(USceneComponent* MotionController);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category=MCAbility)
    void OnGrabActor(USceneComponent* MotionController, AActor* GrabbedActor);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category=MCAbility)
    void OnGrabRelease(USceneComponent* MotionController);
	
public:
};

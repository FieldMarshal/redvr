// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MCAbility.h"
#include "MCAbilityTickable.h"
#include "MCAbilityGrabber.h"
#include "MCAbilityManageable.h"
#include "Components/ActorComponent.h"
#include "REDVR/Utilities/InterfaceExecuteContainer.h"

#include "MCAbilityManager.generated.h"


class IMCAbilityGrabber;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class REDVR_API UMCAbilityManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMCAbilityManager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/**
	* Ticks All IMCTickable Abilities being Managed
	*/
	void TickManagedAbilities();

	/**  Handle  Grabbing */
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGrabbedActorDelegate);
	UPROPERTY( BlueprintCallable, Category = "MCActions");
	FGrabbedActorDelegate OnGrabbed;

	/**  Handle  Grab Action */
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent, Category = "MCActions")
    void OnGrab(USceneComponent* MotionController);

	/**  Handle  Grab an Actor Action*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category=MCAbility)
    void OnGrabActor(USceneComponent* MotionController, AActor* GrabbedActor);
	
	/**  Handle  Grab Release Action*/
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent, Category = "MCActions")
	void OnGrabRelease(USceneComponent* MotionController);

	/** Assign MC to all components that use them*/
	UFUNCTION(BlueprintCallable, Category = "MCAbility")
    void SetMotionControllers(USceneComponent* LeftHand, USceneComponent* RightHand);

	UFUNCTION(BlueprintCallable, Category = "MCAbility")
    void AddAbility(USceneComponent* AbilityComponent);

	APawn* GetMCOwner();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Abilities")
	TArray<FString> ManagedAbilityNames;
	
private:
	/**
	 * Stores all owner components of type ability in an array
	 * Only* use after component is initialized
	 */
	bool AddOwnerAbilities();
	
	/**
	 * Store Ability and its Name
	 * @param Ability Ability to Manage
	 * @param AbilityObj 
	 */
	bool AddAbility(IMCAbilityManageable* Ability, UObject* AbilityObj);

	/**  Abilities that can be ticked */
	bool TrySetTickableAbilities();

	/** Abilities that have Grab events */
	bool TrySetGrabbableAbilities();

	/** Abilities Assign */
	// template<typename T>
	// bool TrySetAbilities(T s);
private:
	APawn* MCOwnerPawn;

	TArray<InterfaceExecuteContainer<IMCAbilityManageable>*> ManageableAbilities;
	
	TArray<InterfaceExecuteContainer<IMCAbilityGrabber>*> GrabberAbilities;
	
	TArray<InterfaceExecuteContainer<IMCAbilityTickable>*> TickableAbilities;
	
	TArray<UActorComponent*> AbilitiesObj;

};


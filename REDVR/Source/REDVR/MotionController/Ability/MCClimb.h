// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MCAbility.h"
#include "MCClimb.generated.h"


UCLASS( ClassGroup=(PawnMotionControllerAbility), meta=(BlueprintSpawnableComponent) )
class REDVR_API UMCClimb : public UMCAbility
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMCClimb();

	void GetActorNearHand();
	
	void OffsetOwnerPawn();
	
protected:
	
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	
	bool isGrabPressed;
	bool isClimbableActor;
		
};

﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MCAbilityGrabber.h"
#include "MCAbilityTickable.h"
#include "UObject/Interface.h"
#include "MCAbilityManageable.generated.h"

// This class does not need to be modified.
UINTERFACE()
class UMCAbilityManageable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class REDVR_API IMCAbilityManageable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent, Category=MCAbility)
	FName GetName();

	UFUNCTION(BlueprintCallable,BlueprintNativeEvent, Category=MCAbility)
	bool SetMC(USceneComponent* LeftHand, USceneComponent* RightHand);

};

// Fill out your copyright notice in the Description page of Project Settings.


#include "MCAbilityManager.h"

// Sets default values for this component's properties
UMCAbilityManager::UMCAbilityManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UMCAbilityManager::BeginPlay()
{
	Super::BeginPlay();

	// ...
	AddOwnerAbilities();
	
}


// Called every frame
void UMCAbilityManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	TickManagedAbilities();
}

void UMCAbilityManager::TickManagedAbilities()
{
	// Execute Tick method on all Managed Abilities
	if(TickableAbilities.Num() > 0)
	{
		for (int i = 0; i < TickableAbilities.Num(); i++)
		{
			TickableAbilities[i]->InterfaceForExecuting->Execute_TickAbility(TickableAbilities[i]->ObjectToExecuteOn);
		}
	}
}

APawn* UMCAbilityManager::GetMCOwner()
{
	if(MCOwnerPawn == nullptr)
	{
		MCOwnerPawn = Cast<APawn>(GetOwner());
	}

	return  MCOwnerPawn;
}

bool UMCAbilityManager::AddAbility(IMCAbilityManageable* Ability, UObject* AbilityObj)
{
	ManageableAbilities.Add ( new InterfaceExecuteContainer<IMCAbilityManageable>{ Ability, AbilityObj });

	FString AbilityName = Ability->Execute_GetName(AbilityObj).ToString();

	AbilityObj->Rename(ToCStr(AbilityName));
	
	ManagedAbilityNames.Add(AbilityName);

	return true;
}

bool UMCAbilityManager::TrySetTickableAbilities()
{
	bool IsAnyAbilitySet = false;
	
	if(AbilitiesObj.Num() > 0)
	{
		// Cast to Get Ability Interface And Object to Be called
		for (int i = 0; i < AbilitiesObj.Num(); i++)
		{
			const auto Ability = Cast<IMCAbilityTickable>(AbilitiesObj[i]);

			
			
			if(Ability != nullptr)
			{
				TickableAbilities.Add(new InterfaceExecuteContainer<IMCAbilityTickable> { Ability, AbilitiesObj[i] });
				IsAnyAbilitySet = true;
			}
		}		
	}
	
	return IsAnyAbilitySet;
}

bool UMCAbilityManager::TrySetGrabbableAbilities()
{
	bool IsAnyAbilitySet = false;
	
	if(AbilitiesObj.Num() > 0)
	{
		// Cast to Get Ability Interface And Object to Be called
		for (int i = 0; i < AbilitiesObj.Num(); i++)
		{
			const auto Ability = Cast<IMCAbilityGrabber>(AbilitiesObj[i]);
			
			if(Ability != nullptr)
			{
				GrabberAbilities.Add(new InterfaceExecuteContainer<IMCAbilityGrabber>{ Ability, AbilitiesObj[i] });
				IsAnyAbilitySet = true;
			}
		}
	}
	
	return IsAnyAbilitySet;
	
}

bool UMCAbilityManager::AddOwnerAbilities()
{
	// TODO: Perform null/type Checks
	AbilitiesObj = GetMCOwner()->GetComponentsByInterface(UMCAbilityManageable::StaticClass());

	bool IsAbilitySetSuccess = AbilitiesObj.Num() > 0;
	
	if(IsAbilitySetSuccess)
	{		
		// Cast to Get Ability Interface
		for (auto AbilityComponent : AbilitiesObj)
		{
			auto Ability = Cast<IMCAbilityManageable>(AbilityComponent);
	
			if(Ability != nullptr)
			{
				AddAbility(Ability, AbilityComponent);
				IsAbilitySetSuccess = true;
			}
		}

		if(IsAbilitySetSuccess)
		{
			TrySetTickableAbilities();
			TrySetGrabbableAbilities();

			return IsAbilitySetSuccess;
		}
	}

	return IsAbilitySetSuccess;
}
	

void UMCAbilityManager::OnGrabActor_Implementation(USceneComponent* MotionController, AActor* GrabbedActor)
{
	if(GrabberAbilities.Num() > 0)
	{
		for (int i = 0; i < GrabberAbilities.Num(); i++)
		{
			GrabberAbilities[i]->InterfaceForExecuting->Execute_OnGrabActor(GrabberAbilities[i]->ObjectToExecuteOn, MotionController, GrabbedActor);
		}
	}
}

void UMCAbilityManager::SetMotionControllers(USceneComponent* LeftHand, USceneComponent* RightHand)
{
	if(ManageableAbilities.Num() > 0)
	{
		for (int i = 0; i < ManageableAbilities.Num(); i++)
		{
			if(ManageableAbilities[i]->InterfaceForExecuting)
			{
				ManageableAbilities[i]->InterfaceForExecuting->Execute_SetMC(ManageableAbilities[i]->ObjectToExecuteOn, LeftHand, RightHand);
			}
		}
	}
}

void UMCAbilityManager::AddAbility( USceneComponent* AbilityComponent)
{
	IMCAbilityManageable* Ability = Cast<IMCAbilityManageable>(AbilityComponent);
	
	if(Ability != nullptr)
	{
		AddAbility(Ability, AbilityComponent);
	}
}


void UMCAbilityManager::OnGrab_Implementation(USceneComponent* MotionController)
{
	if(GrabberAbilities.Num() > 0)
	{
		for (int i = 0; i < GrabberAbilities.Num(); i++)
		{
			GrabberAbilities[i]->InterfaceForExecuting->Execute_OnGrab(GrabberAbilities[i]->ObjectToExecuteOn, MotionController);
		}
	}
}

void UMCAbilityManager::OnGrabRelease_Implementation(USceneComponent* MotionController)
{
	if(GrabberAbilities.Num() > 0)
	{
		for (int i = 0; i < GrabberAbilities.Num(); i++)
		{
			GrabberAbilities[i]->InterfaceForExecuting->Execute_OnGrabRelease(GrabberAbilities[i]->ObjectToExecuteOn, MotionController);
		}
	}
}




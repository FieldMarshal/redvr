﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "DragDriver.h"

DragDriver::DragDriver()
{
	
}

void DragDriver::AddDriverComponent(const USceneComponent* DriverComponent)
{
	DriverSet.Add(DriverComponent);
}

void DragDriver::RemoveDriverComponent(const USceneComponent* DriverComponent)
{
	DriverSet.Remove(DriverComponent);
}

FVector DragDriver::GetDragDriverAverageVelocity()
{
	FVector InBetweenVector =  FVector::ZeroVector;
	
	for (auto Driver : DriverSet)
	{
		InBetweenVector += Driver->GetComponentVelocity();
	}

	return ( InBetweenVector / DriverSet.Num() );
}

FVector DragDriver::GetCurrentPositionVector()
{
	FVector InBetweenVector =  FVector::ZeroVector;
	
	for (auto Driver : DriverSet)
	{
		InBetweenVector += Driver->GetComponentLocation();
	}
	
	return ( InBetweenVector / DriverSet.Num() );
}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "DragDriver.h"
#include "MCAbility.h"
#include "MCDragMove.generated.h"

/**
 * 
 */
UCLASS( ClassGroup=(PawnMotionControllerAbility), meta=(BlueprintSpawnableComponent) )
class REDVR_API UMCDragMove : public UMCAbility
{
	GENERATED_BODY()
public:
	// Sets default values for this component's properties
	UMCDragMove();
	
	UFUNCTION(BlueprintCallable, Category = "Data")
    void SetEnableDragMovement(bool bIsEnabled);
	
	UFUNCTION(BlueprintCallable, Category = "Data")
    void SetEnableDragMovementSingleHand(bool bIsEnabled, FVector OriginalHandPositionVector);

	UFUNCTION(BlueprintCallable, Category = "Data")
    void EnableDragMovement(bool bIsEnabled, const USceneComponent* DragDriver);
	
private:
	void SetDragStartTime(float StartTime);
	
	void DragMove();

	FVector GetDragAcceleration();

	float GetDistanceFromDragStart();

	float GetCurrentTime();

	float GetElapsedTime();
	
private:
	DragDriver ADragDriver;

	bool bIsDragMovementEnabled;

	FVector OriginalHandPositionVector;
	
	FVector OriginalHandVelocityVector;
	
	FVector PreviousHandVelocityVector = FVector::ZeroVector;
	
	FVector DragMoveDirection;

	float DragStartTime;
	
	float DistanceFromDragStart;

};

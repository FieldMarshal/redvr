// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MCAbility.h"
#include "MCAbilityGrabber.h"
#include "MCAbilityManageable.h"
#include "MCAbilityTickable.h"

#include "MCScale.generated.h"


UCLASS( ClassGroup=(PawnMotionControllerAbility), meta=(BlueprintSpawnableComponent) )
class REDVR_API UMCScale : public UMCAbility, public IMCAbilityManageable, public IMCAbilityGrabber, public  IMCAbilityTickable
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMCScale();
	
	UFUNCTION(BlueprintCallable, Category = "Data")
	void SetTargetToScale(AActor* targetToScale);
	
	void ScaleTarget(USceneComponent* targetToScale);
	
	void ScaleTarget(AActor* targetToScale);

	UFUNCTION(BlueprintCallable,BlueprintNativeEvent, Category = "MCActions")
    void OnGrab(USceneComponent* MotionController);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category=MCAbility)
    void OnGrabActor(USceneComponent* MotionController, AActor* GrabbedActor);
	
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent, Category = "MCActions")
    void OnGrabRelease(USceneComponent* MotionController);

	UFUNCTION(BlueprintCallable,BlueprintNativeEvent, Category=MCAbility)
	bool SetMC(USceneComponent* LeftHand, USceneComponent* RightHand);
	
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent, Category=MCAbility)
	FName GetName();

	UFUNCTION(BlueprintCallable,BlueprintNativeEvent, Category = "MCActions")
    void TickAbility();
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	

private:
	// Object to be Scaled
	AActor* TargetActorToScale;

	bool IsTargetScalable;
	
	FVector TargetOriginalScaleVector;

	float OriginalHandDistance = 0.0f;

};

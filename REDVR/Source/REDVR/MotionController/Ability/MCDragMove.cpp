// Fill out your copyright notice in the Description page of Project Settings.


#include "MCDragMove.h"

UMCDragMove::UMCDragMove()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UMCDragMove::SetEnableDragMovement(const bool bIsEnabled)
{
	bIsDragMovementEnabled = bIsEnabled;
}

void UMCDragMove::SetEnableDragMovementSingleHand(const bool bIsEnabled, const FVector StartingHandPosVector)
{
	SetEnableDragMovement(bIsEnabled);

	this->OriginalHandPositionVector = StartingHandPosVector;
}

void UMCDragMove::EnableDragMovement(const bool bIsEnabled, const USceneComponent* DragDriverComponent)
{
	if (bIsEnabled)
	{
		SetDragStartTime(GetCurrentTime());

		ADragDriver.AddDriverComponent(DragDriverComponent);
		
		OriginalHandPositionVector = ADragDriver.GetCurrentPositionVector();
	}
	else
	{
		ADragDriver.RemoveDriverComponent(DragDriverComponent);
	}
	
	SetEnableDragMovement(bIsEnabled);
	
}

void UMCDragMove::DragMove()
{
	const FVector Acceleration = GetDragAcceleration();

	// Get Normal And Inverse Direction So Movement is opposite from drag
	DragMoveDirection = Acceleration.GetSafeNormal() * -1.0f;

	// Sweet Spot Multiplier to make it feel right
	const float SweetSM = 1.0f;
		
	const float MovementScaleValue = Acceleration.Size() * GetDistanceFromDragStart() * SweetSM;
	
	MCOwnerPawn->AddMovementInput(DragMoveDirection, MovementScaleValue, false);
}



FVector UMCDragMove::GetDragAcceleration()
{
	const FVector CurrentVelocity = LeftHandMC->ComponentVelocity;

	const FVector Acceleration = (CurrentVelocity - PreviousHandVelocityVector) / GetElapsedTime(); 

	PreviousHandVelocityVector = CurrentVelocity;
	
	return Acceleration;
}



float UMCDragMove::GetCurrentTime()
{
	// One way to get time span
	float currentTime = GetWorld()->GetTimeSeconds();
	
	//Another way 
	//float currentTime = MCOwnerPawn->GetGameTimeSinceCreation();

	return  currentTime;
}

float UMCDragMove::GetElapsedTime()
{
	return GetCurrentTime() - DragStartTime;
}

float UMCDragMove::GetDistanceFromDragStart()
{
	return  FVector::Distance(OriginalHandPositionVector, ADragDriver.GetCurrentPositionVector());
}

void UMCDragMove::SetDragStartTime(const float StartTime)
{
	this->DragStartTime = StartTime;
}


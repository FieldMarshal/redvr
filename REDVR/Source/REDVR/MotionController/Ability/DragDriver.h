﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class REDVR_API DragDriver
{
	
public:
	DragDriver();
	
public:
	void AddDriverComponent(const USceneComponent* DriverComponent);
	
	void RemoveDriverComponent(const USceneComponent* DriverComponent);
	
	FVector GetDragDriverAverageVelocity();

	FVector GetCurrentPositionVector();
	
private:
	TSet<const USceneComponent*> DriverSet;

};
